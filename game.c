/*
The Pokem@ns Game
Written by: Kevin Scrivnor
For COMP/IT 421
Modified by: Fred Contrata 27/03/2014
To compile:
gcc game.c pokemans.c -o pokemans -lncurses
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/msg.h>
#include "pokemans.h"
#include "pokemessage.h"

int main(void) {
    setupGame();

    key_t key;
    int msqid;
    if ((key = ftok(QUEFILE, 'b')) == -1) {
        perror("ftok");
        exit(1);
    }
    if ((msqid = msgget(key, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }

    struct poke_msgbuf pmb;

    while(1) {
        //printf("waiting for message\n");
        if (msgrcv(msqid, &pmb, sizeof(struct poke_msgbuf) - sizeof(long), 0, 0) == -1) {
            perror("msgrcv");
            exit(1);
        }
        //printf("%s %d\n", pmb.mname, pmb.mcommand);
        moveDude(pmb.mcommand, pmb.mname);
    } 

    endwin();
    printf("Thanks for playing!\n");
    msgctl(msqid, IPC_RMID, NULL);

    return 0;
}
