#!/bin/bash

INRES="570x336" # input resolution
FPS="5" # target FPS
QUAL="ultrafast"  # one of the many FFMPEG preset
#$DISPLAY

ffmpeg -f x11grab -s "$INRES" -r "$FPS" -i $DISPLAY+0,40 \
   -vcodec libx264 -crf 40 -preset "$QUAL" -s "$INRES" -threads 0 \
   -pix_fmt yuv420p \
   -f flv "rtmp://live.justin.tv/app/yourstreamkeyhere" 

