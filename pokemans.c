/*
 The Pokem@ns Game
 
 Written by: Kevin Scrivnor
 For COMP/IT 421
*/

#include <stdio.h>
#include <curses.h>
#include <stdlib.h>
#include <sys/time.h>
#include <unistd.h>
#include <string.h>
#include "pokemans.h"

typedef struct {
    int x;
    int y;
    int money;
    int money_x;
    int money_y;
    int bad_x[BAD_COUNT];
    int bad_y[BAD_COUNT];
    int bads;
    int lives;
    int record;
    int command;
    char commander[32];
} GAME_INFO;

GAME_INFO game = { .record = 0};
char *command[] = { "none", "up", "down", "left", "right" };

void initGameData(void) {
    // set up initial game info
    game.x = ROWS/2;    // start DUDE in middle
    game.y = COLS/2;
    game.money = 0;     // no monies D:
    game.lives = START_LIVES;     // 3 lives!
    game.bads = 0;      // 0 bad guys for now
    game.command = 0;   // no commands yet
}

void setupGame(void) {
    initGameData();
    
    // set up the ncurses screen
    initscr();
    clear();
    // do not display the cursor
    curs_set(0);
    noecho();
	cbreak();
    
    // draw a DUDE in the middle of the screen
    drawDude(5,5,game.x, game.y);
    // draw an ITEM at a random place on the screen
    drawItem();
    // draw some BADs
    int i;
    for(i = 0; i < START_BADS; i++) drawBad();

    // show info bar
    writeInfo();

    // draw everything to window
    refresh();
}

void writeInfo(void) {
    move(0,0); // move to top right
    clrtoeol(); // clear line
    // print info bar
    printw("@: %d | $: %d | x,y: %d,%d | record $: %d | %s says %s ", game.lives, game.money, game.x, game.y, game.record, game.commander, command[game.command]);
    
    // write to window
    refresh();
}

int drawDude(int x, int y,int tx, int ty) {
    chtype ch;
    
    // move to original position of DUDE
    move(x,y);
    // remove him
    addch(' ');
    // move cursor to new position
    move(tx,ty);
    // get current character
    ch = inch();

    // add DUDE there
    move(tx,ty);
    addch(POKEMAN);
    
    // return item
    return isNotBlank(ch);
}

void drawItem(void) {
    struct timeval tv;
    chtype ch;

    do {
    	// get milliseconds
        gettimeofday(&tv, NULL);
        // seed random with milliseconds
        srandom(tv.tv_usec);
        // calculate random location
        game.money_x = (random() % ROWS -1) + 1;
        game.money_y = (random() % COLS -1) + 1;
        // make sure the x is not on our menu bar
        if(!game.money_x) game.money_x++;
        // move there
        move(game.money_x,game.money_y);
        // get character
        ch = inch();
        // do while the character is not a blank space
    } while( isNotBlank(ch) );
    
    // add it
    addch(ITEM);
}

// same as item
void drawBad(void) {
    struct timeval tv;
    chtype ch;
    int i;
  
    if(game.bads >= BAD_COUNT)
        return;
    
    i = ++game.bads;
    do {
        gettimeofday(&tv, NULL);
        srandom(tv.tv_usec);
        game.bad_x[i] = (random() % ROWS -1) + 1;
        game.bad_y[i] = (random() % COLS -1) + 1;
        move(game.bad_x[i],game.bad_y[i]);
        ch = inch();
    } while( isNotBlank(ch) );

    addch(BAD);
}

void removeBad(int x, int y) {
    int i;
    // find the bad at x and y
    for (i = 0; i < game.bads; i++) {
        if(game.bad_x[i] == x && game.bad_y[i] == y) {
        	// replace it with the last one
            game.bad_x[i] = game.bad_x[game.bads-1];
            game.bad_y[i] = game.bad_y[game.bads-1];
            game.bads--;
            return;
        }
    }
    return;
}

int moveDude(int new_command, char *commander) {
    int x = game.x;
    int y = game.y;
    int error = 0, item = 0, i;
    
    // save the command
    game.command = new_command;
    // save the commander
    strncpy(game.commander, commander, 32);

	// check if move is legal (within ROWS and COLS)
    switch(game.command) {
        case 1: // move up in the world
            game.x--;
            if(game.x == 0 || game.x == ROWS) {
                game.x++;
                error = 1;
            }
            break;
        case 2: // move down in the world
            game.x++;
            if(game.x == 0 || game.x == ROWS) {
                game.x--;
                error = 1;
            }
            break;
        case 3: // move left
            game.y--;
            if(game.y == 0 || game.y == COLS) {
                game.y++;
                error = 1;
            }
            break;
        case 4: // move right
            game.y++;
            if(game.y == 0 || game.y == COLS) {
                game.y--;
                error = 1;
            }
            break;
    }
    
    // error!
    if(error == 1){
        move(0,0);
        clrtoeol();
        printw("ERROR: Can't move that way %s!", commander);
        refresh();
        sleep(1);
        writeInfo();
        return -1;
    }

	// if no errors, move the DUDE    
    item = drawDude(x,y,game.x, game.y);
    // check for item or bad
    switch(item) {
        case 2: // found a BAD
            // lose a life D:
            game.lives--;
            // remove the bad from bad list
            removeBad(game.x,game.y);
            // add five more!
            for(i = 0; i < NEW_BADS; i++) drawBad();
            break;
        case 3: // found a ITEM
            // get money :D
            game.money++;
            // make more money!
            drawItem();
            break;
    }
    // update the info
    writeInfo();
    // check to see if the game is over
    checkForLoss();
    
    return 1;
}

// game over man, game over.
void checkForLoss(void) {
    if(game.lives==0){ // GAME OVER D:
        if(game.record < game.money) // save record if highest
        game.record = game.money;
        int i;
        for(i = 10; i > 0; i--) { // countdown to reset
            move(0,0);
            clrtoeol();
            printw("DEFEAT! RESETTING IN %d SECONDS", i);
            refresh();
            sleep(1);
        }
        setupGame(); // call setup game to reset
        return;
    }
}

// return character type
int isNotBlank(chtype ch) {
    char c = ch & A_CHARTEXT;
    switch(c) {
        case POKEMAN:
            return 1;
        case BAD:
            return 2;
        case ITEM:
            return 3;
        default:
            return 0;
    }
}
