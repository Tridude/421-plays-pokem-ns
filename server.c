/*
IRC bot for pokemans
Modified by: Fred Contrata 27/03/2014
IRC code stolen from dav7: https://bbs.archlinux.org/viewtopic.php?id=64254
*/

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <netdb.h>
#include <stdarg.h>
#include <sys/msg.h>
#include "pokemessage.h"

int conn;
char sbuf[512];

void raw(char *fmt, ...) {
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(sbuf, 512, fmt, ap);
    va_end(ap);
    printf("<< %s", sbuf);
    write(conn, sbuf, strlen(sbuf));
}

int main(int argc, char *argv[]) {
    
    key_t key;
    int msqid;
    if ((key = ftok(QUEFILE, 'b')) == -1) {
        perror("ftok");
        exit(1);
    }
    if ((msqid = msgget(key, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    struct poke_msgbuf pmb;
    pmb.mtype = 1;
    int userLen;

    char *nick = "yournickhere";
    char *channel = "#yournickhere";
    char *host = "irc.twitch.tv";
    char *port = "6667";
    char *password = "yourpasshere";
    
    char *user, *command, *where, *message, *sep, *target;
    int i, j, l, sl, o = -1, start, wordcount;
    char buf[513];
    struct addrinfo hints, *res;
    
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(host, port, &hints, &res);
    conn = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    connect(conn, res->ai_addr, res->ai_addrlen);
    
    //raw("USER %s 0 0 :%s\r\n", nick, nick);
    raw("PASS %s\r\n", password); // sent in plain text?
    raw("NICK %s\r\n", nick);
    
    while ((sl = read(conn, sbuf, 512))) {
        for (i = 0; i < sl; i++) {
            o++;
            buf[o] = sbuf[i];
            if ((i > 0 && sbuf[i] == '\n' && sbuf[i - 1] == '\r') || o == 512) {
                buf[o + 1] = '\0';
                l = o;
                o = -1;
                
                printf(">> %s", buf);
                
                if (!strncmp(buf, "PING", 4)) {
                    buf[1] = 'O';
                    raw(buf);
                } else if (buf[0] == ':') {
                    wordcount = 0;
                    user = command = where = message = NULL;
                    for (j = 1; j < l; j++) {
                        if (buf[j] == ' ') {
                            buf[j] = '\0';
                            wordcount++;
                            switch(wordcount) {
                                case 1: user = buf + 1; break;
                                case 2: command = buf + start; break;
                                case 3: where = buf + start; break;
                            }
                            if (j == l - 1) continue;
                            start = j + 1;
                        } else if (buf[j] == ':' && wordcount == 3) {
                            if (j < l - 1) message = buf + j + 1;
                            break;
                        }
                    }
                    
                    if (wordcount < 2) continue;
                    
                    if (!strncmp(command, "001", 3) && channel != NULL) {
                        raw("JOIN %s\r\n", channel);
                    } else if (!strncmp(command, "PRIVMSG", 7) || !strncmp(command, "NOTICE", 6)) {
                        if (where == NULL || message == NULL) continue;
                        if ((sep = strchr(user, '!')) != NULL) user[sep - user] = '\0';
                        if (where[0] == '#' || where[0] == '&' || where[0] == '+' || where[0] == '!') target = where; else target = user;
                        printf("[from: %s] [reply-with: %s] [where: %s] [reply-to: %s] %s", user, command, where, target, message);
                        //raw("%s %s :%s", command, target, message); // If you enable this the IRCd will get its "*** Looking up your hostname..." messages thrown back at it but it works...
                        if (!strncmp(message, "UP", 2)) {
                            pmb.mcommand = 1;
                        } else if (!strncmp(message, "DOWN", 4)) {
                            pmb.mcommand = 2;
                        } else if (!strncmp(message, "LEFT", 4)) {
                            pmb.mcommand = 3;
                        } else if (!strncmp(message, "RIGHT", 4)) {
                            pmb.mcommand = 4;
                        } else {
                            continue;
                        }
                        userLen = strlen(user);
                        if (userLen >= 64) {
                            userLen = 63;
                        }
                        strncpy(pmb.mname, user, userLen);
                        pmb.mname[userLen] = '\0';
                        if (msgsnd(msqid, &pmb, sizeof(struct poke_msgbuf) - sizeof(long), 0) == -1) {
                            perror("msgsnd");
                        } else {
                            printf("Sent %s %d\n", pmb.mname, pmb.mcommand);
                        }
                    }
                }
                
            }
        }
        
    }
    
    return 0;
    
}