
#ifndef __POKEMSG__
#define __POKEMSG__

#define QUEFILE "pokeque"

struct poke_msgbuf {
    long mtype;
    int mcommand;
    char mname[64];
};

#endif
