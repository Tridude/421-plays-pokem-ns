/*
 The Pokem@ns Game
 
 Written by: Kevin Scrivnor
 For COMP/IT 421
 */

#ifndef __POKEMANS__
#define __POKEMANS__
#include <curses.h>

#define ROWS 24
#define COLS 80
#define POKEMAN '@'
#define ITEM '$'
#define BAD '%'
#define BAD_COUNT 128
#define START_LIVES 3
#define START_BADS 5
#define NEW_BADS 3

void initGameData(void);
void setupGame(void);

void writeInfo(void);
int drawDude(int,int,int,int);
void drawItem(void);
void drawBad();

void removeBad(int, int);

int moveDude(int, char*);
void checkForLoss(void);
int isNotBlank(chtype);

#endif
